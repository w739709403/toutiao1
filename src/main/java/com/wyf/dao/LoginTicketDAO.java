package com.wyf.dao;

import com.wyf.model.LoginTicket;
import com.wyf.model.User;
import org.apache.ibatis.annotations.*;

import static com.wyf.dao.NewsDAO.INSERT_FIELDS;

@Mapper
public interface LoginTicketDAO {
    String TABLE_NAME = "login_ticket";
    String INSET_FIELDS = " user_id, expired,status, ticket ";
    String SELECT_FIELDS = " id,"+INSET_FIELDS;

    @Insert({"insert into ", TABLE_NAME, "(", INSET_FIELDS,
            ") values (#{userId},#{expired},#{status},#{ticket})"})
    int addTicket(LoginTicket ticket);

//    @Select({"select ", SELECT_FIELDS, " from ", TABLE_NAME, " where id=#{id}"})
//    User selectById(int id);
//
//    @Update({"update ", TABLE_NAME, " set password=#{password} where id=#{id}"})
//    void updatePassword(User user);
//
//    @Delete({"delete from ", TABLE_NAME, " where id=#{id}"})
//    void deleteById(int id);
    @Select({"select",SELECT_FIELDS,"from",TABLE_NAME,"where ticket =#{ticket}"})
    LoginTicket selectByTicket(String ticket);


    @Update({"update",TABLE_NAME,"set status=#{status} where ticket =#{ticket}"})
    void updateStatus(@Param("ticket")String ticket,@Param("status")int status);

}
