package com.wyf.model;

import org.springframework.aop.target.ThreadLocalTargetSource;
import org.springframework.stereotype.Component;

/**
 * Created by w7397 on 2016/8/7.
 */
@Component
public class HostHolder {
    private static ThreadLocal<User> users=new ThreadLocal<User>();

    public User getUser(){
        return users.get();
    }
    public void  setUser(User user){
        users.set(user);
    }
    public void  clear(){
        users.remove();
    }
}
