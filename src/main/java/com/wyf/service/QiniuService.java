package com.wyf.service;

import com.alibaba.fastjson.JSONObject;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.wyf.util.ToutiaoUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by w7397 on 2016/8/7.
 */
@Service
public class QiniuService {
    private static final Logger logger = LoggerFactory.getLogger(ToutiaoUtil.class);
    String ACCESS_KEY = "dWTQaBlJjBumwbH1rthuLNPk9UcGkpANTaL6zxLS";
    String SECRET_KEY = "_xIyyWrjhUujOvTVYiOXGTvRKGESRVCVcuDPvxf4";
    //要上传的空间
    String bucketname = "wyfimage";
    //上传到七牛后保存的文件名
    String key = "my-java.png";
    //密钥配置
    Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
    //创建上传对象
    UploadManager uploadManager = new UploadManager();

    //简单上传，使用默认策略，只需要设置上传的空间名就可以了
    public String getUpToken(){
        return auth.uploadToken(bucketname);
    }

    public String saveImage(MultipartFile file) throws IOException {
        try {
            int dotsPos= file.getOriginalFilename().lastIndexOf(".");
            if(dotsPos<0){
                return null;
            }
            String fileExt=file.getOriginalFilename().substring(dotsPos+1).toLowerCase();
            if(!ToutiaoUtil.isFileAllowed(fileExt)){
                return null;
            }

            String fileName = UUID.randomUUID().toString().replaceAll("-", "") + "." + fileExt;
            //调用put方法上传
            Response res = uploadManager.put(file.getBytes(),fileName,getUpToken());
            //打印返回的信息
            System.out.println(res.toString());
            //return null;
            if(res.isOK() && res.isJson()){
                String key=JSONObject.parseObject(res.bodyString()).get("key").toString();
                return ToutiaoUtil.QINIU_DOMAIN_PREFIX+key;
            }else {
                logger.error("七牛异常"+res.bodyString());
                return null;
            }
        } catch (QiniuException e) {
           logger.error("七牛异常"+e.getMessage());
            return null;
        }
    }
}
