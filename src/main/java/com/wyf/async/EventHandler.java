package com.wyf.async;

import java.util.List;

/**
 * Created by w7397 on 2016/8/9.
 */
public interface EventHandler {
    void doHandle(EventModel moedl);
    List<EventType>getSupportEventTypes();
}
