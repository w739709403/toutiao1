package com.wyf.async.handler;

import com.wyf.async.EventHandler;
import com.wyf.async.EventModel;
import com.wyf.async.EventType;
import com.wyf.model.Message;
import com.wyf.model.User;
import com.wyf.service.MessageService;
import com.wyf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by w7397 on 2016/8/9.
 */
@Component
public class LikeHandler implements EventHandler {

    @Autowired
    UserService userService;

    @Autowired
    MessageService messageService;

    @Override
    public void doHandle(EventModel model) {
        Message message=new Message();
        message.setFromId(3);
        //message.setToId(model.getEntityOwnerId());
        message.setToId(model.getActorId());
        User user=userService.getUser(model.getActorId());
        message.setContent("用户"+user.getName()
        +"赞了你的资讯,http://127.0.0.1:8080/news/"+model.getEntityId());
        message.setCreatedDate(new Date());
        messageService.addMessage(message);
    }

    @Override
    public List<EventType> getSupportEventTypes() {
        return Arrays.asList(EventType.LIKE);
    }
}
